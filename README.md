# Under construction

# YAAE - Yet another audio engine


## Next steps:

- BezierVolume
- Brownian noises
- PanAutomation
- PanVolume
- PanLeftToRight
- PanRightToLeft
- PanCross
- BezierPan
- Distortion
- FFT
- Filter IIR
- PinkNoise
- OtherNoises
- Fix noise in tremolo

## Example of use:

 ~~~
let audioNode = AudioNode();

audioNode += FunctionSignal(frequency: 110, volume: 0.5, signal: SignalType.sine)
audioNode += HarmonicSignal(frequency: 110, volumes: 0.5, 0.1, 0.2)
audioNode += ADSR(attack: VolumeControl(timeInMillis: 15, volume: 1),
decay: VolumeControl(timeInMillis: 30, volume: 0.4),
sustain: VolumeControl(timeInMillis: 2500, volume: 0.7),
release: VolumeControl(timeInMillis: 2700, volume: 0.0))
audioNode += Tremolo(wave: 0.2, rate: 0.4, depth: 0.8)
audioNode += WhiteNoise(intensity: 0.01, volume: 0.6)
audioNode += CleanDigitalAmplifier(volume: 0.5)

guard let pcmData = PCMFormat().buffer(streamDescription: StreamDescription().pcm(), durationInSeconds: 3, sampleBuilder: audioNode) else { exit(0) }
guard let wavData = WAVFormat().create(data: pcmData, streamDescription: StreamDescription().pcm()) else { exit(0) }

do {

let player = try AVAudioPlayer(data: wavData, fileTypeHint: AVFileType.wav.rawValue)

player.prepareToPlay()
player.play()

while (player.isPlaying) {
    print("Waiting...")
    sleep(1)
}

} catch let error {
    print(error.localizedDescription)
}
~~~


