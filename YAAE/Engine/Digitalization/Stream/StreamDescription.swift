//
//  StreamConfig.swift
//  PCMGen
//
//  Created by Thiago Santos on 01/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation
import AVFoundation


struct StreamDescription {
    
    func pcm(sampleRate: Float64 = 48000, bitDepth: UInt32 = 32, channels: UInt32 = 1) -> AudioStreamBasicDescription {
        
        let bytes: UInt32 = UInt32((bitDepth / 8) * channels);
        
        return AudioStreamBasicDescription(
            mSampleRate: sampleRate,
            mFormatID: kAudioFormatLinearPCM,
            mFormatFlags:  kAudioFormatFlagIsSignedInteger | kAudioFormatFlagsNativeEndian | kAudioFormatFlagIsPacked,
            mBytesPerPacket: bytes,
            mFramesPerPacket: 1,
            mBytesPerFrame: bytes,
            mChannelsPerFrame: 4,
            mBitsPerChannel: bitDepth,
            mReserved: 0);
    }
}
