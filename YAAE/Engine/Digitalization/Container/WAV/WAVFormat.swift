//
//  WAVFormat.swift
//  PCMGen
//
//  Created by Thiago Santos on 01/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation
import AVFoundation


struct WAVFormat {
    
    func create(data: Data, streamDescription: AudioStreamBasicDescription) -> Data? {
        //From: http://soundfile.sapp.org/doc/WaveFormat/
        
        let riffInAscii = 0x52494646;
        let waveInAscii = 0x57415645;
        let fmtInAscii = 0x666d7420;
        let dataInAscii = 0x64617461;
        
        var file = Data()
        
        // Chunk ID 'RIFF' in ascii little endian (OS reverses)
        file.append(UInt32(riffInAscii).bigEndian)
        // ChunkSize
        file.append(UInt32(data.count + 36))
        //Format 'WAVE' in ascii little endian
        file.append(UInt32(waveInAscii).bigEndian)
        // Subchunk1ID = 'fmt '
        file.append(UInt32(fmtInAscii).bigEndian)
        // SubchunckSize = 16 or 0x10 for PCM
        file.append(UInt32(16))
        // AudioFormat, PCM format = 1
        file.append(UInt16(1))
        // NumChannels
        file.append(UInt16(streamDescription.mChannelsPerFrame))
        // SampleRate
        file.append(UInt32(streamDescription.mSampleRate))
        // ByteRate
        file.append(UInt32(UInt32(streamDescription.mSampleRate) * streamDescription.mBitsPerChannel * streamDescription.mChannelsPerFrame / 8))
        // BlockAlign (bytesPerSample)
        file.append(UInt16(streamDescription.mBytesPerFrame))
        // BitsPerSample
        file.append(UInt8(streamDescription.mBitsPerChannel))
        // ExtraParamSize if PCM, then doesn't exist
        file.append(UInt8(0))
        // Subchunk2ID = 'data'
        file.append(UInt32(dataInAscii).bigEndian)
        // SubChunkSize = NumSamples * NumChannels * BitsPerSample/8. This is the number of bytes in the data.
        file.append(UInt32(data.count))
        //Add sound data
        file.append(data)
        
        return file;
    }
}
