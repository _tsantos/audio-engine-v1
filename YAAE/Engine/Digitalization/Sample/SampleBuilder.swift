//
//  SampelBuilder.swift
//  PCMGen
//
//  Created by Thiago Santos on 01/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


protocol SampleBuilder {
    
    func buildAtTime(timeInSeconds: Double, currentAmplitude: Double) -> Double
    
}
