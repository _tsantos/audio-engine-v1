//
//  CommonFunctions.h
//  PCMGen
//
//  Created by Thiago Santos on 28/06/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

#ifndef SampleBuilder_h
#define SampleBuilder_h

#if !defined(VOLUME_LIMIT)
#define HEADROOM 0.33
#define VOLUME_LIMIT INT_MAX * HEADROOM
#endif

@protocol SampleBuilder

- (double)buildAtTime:(float)timeInSeconds currentAmplitude:(double)currentAmplitude;

@end
#endif /* SampleBuilder_h */
