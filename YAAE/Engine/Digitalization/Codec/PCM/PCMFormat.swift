//
//  PCMFormat.swift
//  PCMGen
//
//  Created by Thiago Santos on 01/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation
import AVFoundation


struct PCMFormat {
    
    func buffer(streamDescription: AudioStreamBasicDescription, durationInSeconds: Float, sampleBuilder: AudioNode) -> Data? {
        
        let bytesPerChannel = (streamDescription.mBitsPerChannel / 8)
        let dataLength = UInt32(Float(streamDescription.mSampleRate) * Float(bytesPerChannel) * Float(durationInSeconds))
        var buffer = Data()
        
        for sampleStep in 0...dataLength {

            let timeInSeconds = Double(sampleStep) / Double(streamDescription.mSampleRate) / Double(bytesPerChannel)
            let sample = sampleBuilder.buildAtTime(timeInSeconds: timeInSeconds, currentAmplitude:0.0)
            buffer.append(Int32(abs(sample) > Volume.clipping ? Volume.clipping : sample));
            
        }
        
        return buffer;
    }
}
