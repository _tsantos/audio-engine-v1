//
//  BrownianNoise.swift
//  PCMGen
//
//  Created by Thiago Santos on 04/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


class BrownianNoise: ColoredNoise {
    
    init(intensity: Double, volume: Double) {
        super.init(intensity: intensity, volume: volume, noise: NoiseType.brownian)
    }
}
