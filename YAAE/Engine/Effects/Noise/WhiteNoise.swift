//
//  WhiteNoise.swift
//  PCMGen
//
//  Created by Thiago Santos on 02/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation



class WhiteNoise: ColoredNoise {
    
    init(intensity: Double, volume: Double) {
        super.init(intensity: intensity, volume: volume, noise: NoiseType.white)
    }
}
