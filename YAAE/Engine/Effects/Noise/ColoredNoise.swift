//
//  ColoredNoise.swift
//  PCMGen
//
//  Created by Thiago Santos on 02/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


typealias NoiseGenerator = (_ frequency: Double, _ timeInSeconds: Double) -> Double

struct NoiseType {
    
    static let white: NoiseGenerator = { intensity, volume in
        let random = Double.random(in: 0 ..< 1)
        let amplitude = Double.random(in: -1 ..< 1) * volume
        return random > intensity ? amplitude: 0
    }
    
    static let brownian: NoiseGenerator = { intensity, volume in
        //TODO
        return 0
    }
}

//https://github.com/AllenDowney/ThinkDSP/blob/master/code/noise.py
//https://en.wikipedia.org/wiki/Colors_of_noise
class ColoredNoise: AudioNode {
    
    private let intensity: Double
    private let validation: Validation
    private let volume: Double
    private let noise: NoiseGenerator
    
    init(intensity: Double, volume: Double, noise: @escaping NoiseGenerator) {
        let validation = Validation(module: String(describing: ColoredNoise.self))
        
        self.validation = validation
        self.noise = noise
        self.intensity = 1 - intensity
        self.volume = Volume.normalizedExp(volume)
        
        validation.betweenZeroAndOne(name: "intensity", value: intensity)
        validation.betweenZeroAndOne(name: "volume", value: volume)
    }
    
    override func buildAtTime(timeInSeconds: Double, currentAmplitude: Double) -> Double {
        
        guard self.validation.ok() else { return currentAmplitude }
        
        return super.buildAtTime(timeInSeconds: timeInSeconds, currentAmplitude: currentAmplitude) + noise(self.intensity, self.volume)
    }
}
