//
//  FAdeOutVolume.swift
//  PCMGen
//
//  Created by Thiago Santos on 04/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


class FadeOutVolume: VolumeAutomation {
    
    init(timeInMillis: Double, duration: Double, volume: Double) {
        super.init(controls: [VolumeControl(timeInMillis: 0.0, volume: 1.0), VolumeControl(timeInMillis: timeInMillis, volume: 1.0),
                              VolumeControl(timeInMillis: timeInMillis + duration, volume: volume)])
    }
}
