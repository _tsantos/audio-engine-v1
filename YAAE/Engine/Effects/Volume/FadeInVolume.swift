//
//  FadeInVolume.swift
//  PCMGen
//
//  Created by Thiago Santos on 04/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


class FadeInVolume: VolumeAutomation {
    
    init(timeInMillis: Double, duration: Double, volume: Double) {
        super.init(controls: [VolumeControl(timeInMillis: 0.0, volume: 0.0), VolumeControl(timeInMillis: timeInMillis, volume: 0.0),
                              VolumeControl(timeInMillis: timeInMillis + duration, volume: volume)])
    }
}
