//
//  ADSR.swift
//  PCMGen
//
//  Created by Thiago Santos on 03/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


class ADSR: VolumeAutomation {
    
    init(attack: VolumeControl, decay: VolumeControl, sustain: VolumeControl, release: VolumeControl) {
        let adsr = [attack, decay, sustain, release]
        super.init(controls: adsr)
    }
}
