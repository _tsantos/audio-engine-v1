//
//  FixedVolume.swift
//  PCMGen
//
//  Created by Thiago Santos on 04/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


class FixedVolume: VolumeAutomation {
    
    init(volume: Double) {
        super.init(controls: [VolumeControl(timeInMillis: 0.0, volume: volume)])
    }
}
