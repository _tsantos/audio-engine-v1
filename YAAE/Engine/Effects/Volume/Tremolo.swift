//
//  Tremolo.swift
//  PCMGen
//
//  Created by Thiago Santos on 01/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


class Tremolo: AudioNode {
    
    private let wave: Double
    private let rate: Double
    private let depth: Double
    private let pattern: Array<Double>
    private let validation: Validation
    
    private var patternPosition: Int
    private var patternChange: Double
    
    convenience init(wave: Double, rate: Double, depth: Double, maxTime: Double = 0.05) {
        self.init(wave: wave, rate: rate, depth: depth,
                  pattern: [maxTime * (1 - rate), maxTime * (1 - rate)])
    }
    
    init(wave: Double, rate: Double, depth: Double, pattern: Array<Double>) {
        let validation = Validation(module: String(describing: Tremolo.self))
        
        self.validation = validation
        
        self.wave = wave
        self.rate = 1 - rate
        self.depth = Volume.normalizedExp(1 - depth)

        self.pattern = pattern
        
        self.patternChange = 0
        self.patternPosition = 0
        
        self.validation.betweenZeroAndOne(name: "wave", value: wave)
        self.validation.betweenZeroAndOne(name: "depth", value: depth)
        self.validation.betweenZeroAndOne(name: "rate", value: rate)
        
        pattern.forEach { time in validation.betweenGreaterThanZero(name: "Time", value: time) }
    }
    
    override func buildAtTime(timeInSeconds: Double, currentAmplitude: Double) -> Double {
        
        guard self.validation.ok() else { return currentAmplitude }
        
        let effectDuration = self.pattern[self.patternPosition % self.pattern.count]
        let elapsedTime = timeInSeconds - self.patternChange
        
        if (self.patternPosition % 2 == 0) {
            return self.silence(elapsedTime: elapsedTime, effectDuration: effectDuration,
                                currentAmplitude: currentAmplitude, timeInSeconds: timeInSeconds)
        } else {
            return self.sound(elapsedTime: elapsedTime, effectDuration: effectDuration,
                              currentAmplitude: currentAmplitude, timeInSeconds: timeInSeconds)
        }
    }
    
    private func silence(elapsedTime: Double, effectDuration: Double,  currentAmplitude: Double, timeInSeconds: Double) -> Double{
        if (elapsedTime > effectDuration) {
            self.nextPatternMark(timeInSeconds: timeInSeconds)
        }
        return currentAmplitude * self.depth
    }
    
    private func sound(elapsedTime: Double, effectDuration: Double,  currentAmplitude: Double, timeInSeconds: Double) -> Double{
        let crestTime = (effectDuration * self.wave)
        let timeToAttackAndDecay = (effectDuration - crestTime) / 2

        var amplitude = currentAmplitude

        let attackBegin = 0.0
        let attackEnd = timeToAttackAndDecay
        
        if (elapsedTime > attackBegin && elapsedTime < attackEnd) {
            
            let attackRate = Volume.normalizedExp(elapsedTime / timeToAttackAndDecay)
            amplitude = currentAmplitude * (attackRate < self.depth ? self.depth : attackRate)
            
        }
        
        let decayBegin = timeToAttackAndDecay + crestTime
        let decayEnd = decayBegin + timeToAttackAndDecay
        
        if (elapsedTime > decayBegin && elapsedTime < decayEnd && elapsedTime > 0.0) {
            
            let decayRate = Volume.normalizedExp((timeToAttackAndDecay - (elapsedTime - decayBegin)) / timeToAttackAndDecay)
            amplitude = currentAmplitude * (decayRate < self.depth ? self.depth : decayRate)
            
        } else if (elapsedTime > decayEnd) {
            
            self.nextPatternMark(timeInSeconds: timeInSeconds)
            amplitude = self.depth
            
        }
        return amplitude
    }
    
    private func nextPatternMark(timeInSeconds: Double) {
        self.patternPosition += 1;
        self.patternChange = timeInSeconds;
    }
}
