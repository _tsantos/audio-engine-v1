//
//  ADSREnvelope.swift
//  PCMGen
//
//  Created by Thiago Santos on 01/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation

class VolumeAutomation: AudioNode {
    
    private var volume: Double = 0
    private var actingSince: Double? = nil
    private let controls: Array<VolumeControl>
    
    init(controls: Array<VolumeControl>) {
        self.controls = controls
    }

    func reset() {
        self.volume = 0
        self.actingSince = nil
    }
    
    override func buildAtTime(timeInSeconds: Double, currentAmplitude: Double) -> Double {
        let elapsedTime = timeInSeconds - self.initialTime(timeInSeconds: timeInSeconds)
        let volume = self.volume(elapsedTime: elapsedTime)
        return super.buildAtTime(timeInSeconds:timeInSeconds, currentAmplitude:currentAmplitude * volume)
    }
    
    private func initialTime(timeInSeconds: Double) -> Double {
        if (actingSince == nil) {
            self.actingSince = timeInSeconds
        }
        return self.actingSince ?? timeInSeconds
    }
    
    private func volume(elapsedTime: Double) -> Double {
        var lastControl = VolumeControl(timeInMillis: 0, volume: 0)
        var result: Double? = nil
        
        for control in self.controls {
            if (result == nil && elapsedTime < control.timeInSeconds) {
                result = self.volumeRateForPoint(from: lastControl, to: control, elapsedTime: elapsedTime)
            }
            lastControl = control
        }
        
        return result ?? 0
    }
    
    private func volumeRateForPoint(from: VolumeControl, to: VolumeControl, elapsedTime: Double) -> Double {

        let elapsedTimeSinceLastPoint = elapsedTime - from.timeInSeconds
        let timeOffsetBetweenPoints = to.timeInSeconds - from.timeInSeconds
        let amplitudeOffset = to.volume - from.volume

        if (amplitudeOffset > 0) {
            return Volume.normalizedExp((elapsedTimeSinceLastPoint / timeOffsetBetweenPoints) * amplitudeOffset)
        } else {
            return Volume.normalizedExp(from.volume - (elapsedTimeSinceLastPoint / timeOffsetBetweenPoints) * -amplitudeOffset)
        }
    }
}

