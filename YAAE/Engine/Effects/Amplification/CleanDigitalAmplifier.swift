//
//  CleanDigitalAmplifier.swift
//  PCMGen
//
//  Created by Thiago Santos on 01/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


class CleanDigitalAmplifier: AudioNode {
    
    private let volume: Double;
    private let validation: Validation
    
    init(volume: Double) {
        let validation = Validation(module:String(describing: CleanDigitalAmplifier.self))
        
        self.validation = validation
        self.volume = Volume.normalizedExp(volume)
        
        validation.betweenZeroAndOne(name: "volume", value: volume)
    }
    
    override func buildAtTime(timeInSeconds: Double, currentAmplitude: Double) -> Double {

        guard self.validation.ok() else { return currentAmplitude }
        
        let amplitude = currentAmplitude * Volume.max * self.volume;
        return super.buildAtTime(timeInSeconds:timeInSeconds, currentAmplitude:amplitude);
    }
}
