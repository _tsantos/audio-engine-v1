//
//  HarmonicSignal.swift
//  PCMGen
//
//  Created by Thiago Santos on 01/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


class HarmonicSignal: AudioNode {
    
    private let validation: Validation
    private let frequency: Double
    private let harmonicSines: Array<FunctionSignal>
    
    init(frequency: Double, signal: @escaping SignalGenerator = SignalType.sine, volumes: Double...) {
        let validation = Validation(module: String(describing: HarmonicSignal.self))
        
        self.validation = validation
        self.frequency = frequency
        
        self.harmonicSines = volumes.enumerated().map { harmonic, volume in
            let harmonicFrequency = frequency * Double(harmonic + 1)
            validation.betweenZeroAndOne(name: "amplitude \(harmonicFrequency)", value: volume)
            return FunctionSignal(frequency: harmonicFrequency, volume: volume, signal: signal)
        }
    }
    
    override func buildAtTime(timeInSeconds: Double, currentAmplitude: Double) -> Double {
        
        guard self.validation.ok() else { return currentAmplitude }
        
        var amplitude = currentAmplitude
        for sine in self.harmonicSines {
            amplitude += sine.buildAtTime(timeInSeconds: timeInSeconds, currentAmplitude: 0)
        }
        return amplitude
    }
}
