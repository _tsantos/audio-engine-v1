//
//  SineSignal.swift
//  PCMGen
//
//  Created by Thiago Santos on 01/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


class SineSignal: FunctionSignal {

    init(frequency: Double, volume: Double) {
        super.init(frequency: frequency, volume: volume, signal: SignalType.sine)
    }
}

