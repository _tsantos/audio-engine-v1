//
//  FunctionSignal.swift
//  PCMGen
//
//  Created by Thiago Santos on 02/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


typealias SignalGenerator = (_ angle: Double) -> Double

struct SignalType {
    
    /**
     Improvements
     ------------
     
     In this case, -atan(1 / tan(angle)) / 10  "/ 10" is a empiric attenuation. Maybe because it sounds 1/2 volume due log10 nature of the ear.
     The right value should be 2 since the function  have this interval [-1.5, 1.5], but sounds too lound. Remove this magic number.
     
     Performance can be improved. Trigonometric functions tends to be many times slower than simple operations.
     */
    static let sawtooth: SignalGenerator = { angle in -atan(1 / tan(angle)) / 10 }
    static let sine: SignalGenerator = { angle in sin(angle) }
    static let cosine: SignalGenerator = { angle in cos(angle) }
    static let triangle: SignalGenerator = { angle in asin(sin(angle)) }
    static let square: SignalGenerator = { angle in sin(angle) > 0 ? 1 : -1 }
    
}

class FunctionSignal: AudioNode {
    
    private let frequency: Double
    private let volume: Double
    private let signal: SignalGenerator
    private let validation: Validation
    
    init(frequency: Double, volume: Double, signal: @escaping SignalGenerator) {
        let validation = Validation(module: String(describing: FunctionSignal.self))
        
        self.validation = validation
        self.frequency = frequency
        self.signal = signal
        self.volume = Volume.normalizedExp(volume)
        
        validation.betweenZeroAndOne(name: "amplitude", value: volume)
    }
    
    override func buildAtTime(timeInSeconds: Double, currentAmplitude: Double) -> Double {

        guard self.validation.ok() else { return currentAmplitude }
        let amplitude = currentAmplitude + self.signal(2 * Double.pi * timeInSeconds * self.frequency) * self.volume
        return super.buildAtTime(timeInSeconds: timeInSeconds, currentAmplitude: amplitude)
    }
    
}

