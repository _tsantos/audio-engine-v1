//
//  AudioNode.swift
//  PCMGen
//
//  Created by Thiago Santos on 01/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


class AudioNode: SampleBuilder {
    
    private var nodes = Array<AudioNode>()
    
    func buildAtTime(timeInSeconds: Double, currentAmplitude: Double) -> Double {
        var amplitude = currentAmplitude
        
        for node in self.nodes {
            amplitude = node.buildAtTime(timeInSeconds: timeInSeconds, currentAmplitude: amplitude)
        }
        return amplitude
    }
    
    static func +=(container: AudioNode, node: AudioNode)  {
        container.nodes.append(node)
    }
    
    static func +(container: AudioNode, node: AudioNode) -> AudioNode {
        container.nodes.append(node)
        return container
    }
    
}
