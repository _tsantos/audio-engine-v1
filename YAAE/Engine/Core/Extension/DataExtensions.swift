//
//  Data.swift
//  PCMGen
//
//  Created by Thiago Santos on 01/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


extension Data {
    
    mutating func append(_ number: UInt32){
        var mutable = number
        self.append(UnsafeBufferPointer(start: &mutable, count: 1))
    }
    
    mutating func append(_ number: Int32){
        var mutable = number
        self.append(UnsafeBufferPointer(start: &mutable, count: 1))
    }
    
    mutating func append(_ number: UInt8){
        var mutable = number
        self.append(UnsafeBufferPointer(start: &mutable, count: 1))
    }
    
    mutating func append(_ number: UInt16){
        var mutable = number
        self.append(UnsafeBufferPointer(start: &mutable, count: 1))
    }
    
    mutating func append(_ number: Double){
        var mutable = number
        self.append(UnsafeBufferPointer(start: &mutable, count: 1))
    }
    
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let hexDigits = Array((options.contains(.upperCase) ? "0123456789ABCDEF" : "0123456789abcdef").utf16)
        var chars: [unichar] = []
        chars.reserveCapacity(2 * count)
        for byte in self {
            chars.append(hexDigits[Int(byte / 16)])
            chars.append(hexDigits[Int(byte % 16)])
        }
        return String(utf16CodeUnits: chars, count: chars.count)
    }
}
