//
//  Validation.swift
//  PCMGen
//
//  Created by Thiago Santos on 01/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


class Validation {
    
    private let module: String
    private var valid: Bool
    
    init(module: String) {
        self.module = module
        self.valid = true
    }    
    
    func betweenZeroAndOne(name: String, value: Double) {
        guard self.valid else { return }
        self.valid = value > 0
        if !self.valid {
            print("[\(self.module)] Invalid \(name) parameter it should be between 0 and 1")
        }
    }
    
    func betweenGreaterThanZero(name: String, value: Double) {
        guard self.valid else { return }
        self.valid = value > 0
        if !self.valid {
            print("[\(self.module)] Invalid \(name) parameter it should be greater than 0")
        }
    }
    
    func ok() -> Bool {
        return self.valid
    }
}
