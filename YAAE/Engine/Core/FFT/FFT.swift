//
//  FFT.swift
//  YAAE
//
//  Created by Thiago Santos on 05/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation
import Accelerate

class FFT {
    
    typealias Spectrum = Array<(frequency: Double, amplitude: Double)>
    
    private let bufferSize: UInt
    private let bufferLog2: UInt
    private let fftSetup: FFTSetup?
    
    init() {
        
        self.bufferSize = vDSP_Length(2048 * 32) // Nyquist frequency is more than is possible to ear
        self.bufferLog2 = vDSP_Length(log2(Float(self.bufferSize)))
        self.fftSetup = vDSP_create_fftsetup(vDSP_Length(self.bufferLog2), FFTRadix(kFFTRadix5))
        
    }
    
    func spectrum(audioNode: AudioNode) -> Spectrum {
        
        guard let fftSetup = self.fftSetup else { return [] }
        
        let buffer = self.buffer(audioNode: audioNode, size: Int(self.bufferSize))
        
        // Transforms buffer in complex numbers
        let complex = asComplex(raw: buffer)
        let complexSize = vDSP_Length(complex.count)
        
        // Allocate buffers
        var inputReal = [Float](repeating:0, count: Int(complexSize))
        var inputImaginary = [Float](repeating:0, count: Int(complexSize))
        
        var outputReal = [Float](repeating: 0, count: Int(complexSize))
        var outputImaginary = [Float](repeating: 0, count: Int(complexSize))
        
        //
        /**
         DSPComplex to SplitComplex
         --------------------------
         
         With split complex vectors (the DSPSplitComplex and DSPDoubleSplitComplex types), the real parts of the elements in the vector are stored in one array, and the imaginary parts are stored in a separate array. Thus, as with real arrays, an address stride of 1 addresses every element. Most functions use split complex vectors.
         
         With interleaved complex vectors (the DSPComplex and DSPDoubleComplex types), complex numbers are stored as ordered pairs of floating point or double values. Therefore, a stride of 2 is specified to process every vector element; a stride of 4 is specified to process every other element; and so forth.
         
         https://developer.apple.com/library/archive/documentation/Performance/Conceptual/vDSP_Programming_Guide/About_vDSP/About_vDSP.html
         */
        var input = DSPSplitComplex(realp: &inputReal, imagp: &inputImaginary)
        var output = DSPSplitComplex(realp: &outputReal, imagp: &outputImaginary)
        vDSP_ctoz(complex, 2, &input, 1, complexSize)
        
        //Apply FFT
        vDSP_fft_zrop(fftSetup, &input, 1, &output, 1, vDSP_Length(self.bufferLog2), FFTDirection(kFFTDirection_Forward))

        return asSpectrum(imaginary: outputImaginary, real: outputReal)
    }
    
    private func asSpectrum(imaginary: Array<Float>, real: Array<Float>) -> Spectrum {
        return imaginary.enumerated()
            .filter { $0.element < -1 }
            .map { (index, imaginary) in
                //https://www.gaussianwaves.com/2015/11/interpreting-fft-results-obtaining-magnitude-and-phase-information/
                let amplitude = sqrtf(pow(real[index], 2) + pow(Float(imaginary), 2)) / Float(self.bufferSize)
                return (frequency: Double(index), amplitude: Double(amplitude))
        }
    }
    
    private func asComplex(raw: Array<Float>) -> [DSPComplex] {

        let evenSize = (raw.count % 2 != 0) ? raw.count - 1 : raw.count
        return stride(from: 0, to: evenSize, by: 2).map { index in
            DSPComplex(real: raw[index], imag: raw[index + 1])
        }
    }
    
    private func buffer(audioNode: AudioNode, size: Int) -> Array<Float> {

        var buffer = Array<Float>()
        for time in 1...size {
            buffer.append(Float(audioNode.buildAtTime(timeInSeconds: Double(time)/Double(size), currentAmplitude: 0)))
        }
        return buffer
    }
    
    deinit {
        vDSP_destroy_fftsetup(self.fftSetup)
    }
    
}
