//
//  WavePoint.swift
//  PCMGen
//
//  Created by Thiago Santos on 03/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


struct VolumeControl {
    
    let timeInSeconds: Double
    let volume: Double
    
    init(timeInSeconds: Double, volume: Double) {
        self.timeInSeconds = timeInSeconds
        self.volume = volume
    }
    
    init(timeInMillis: Double, volume: Double) {
        self.timeInSeconds = timeInMillis / 1000
        self.volume = volume
    }
}
