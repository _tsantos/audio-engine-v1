//
//  Volume.swift
//  PCMGen
//
//  Created by Thiago Santos on 01/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


struct Volume {
    
    static let headroom = 0.33
    static let clipping = Double(Int32.max) 
    static let max = Double(Int32.max) * normalizedExp(1 - headroom)
    
    static func normalizedExp(_ value:Double) -> Double {
        return pow(10, value)/10;
    }
    
    static func normalizedLog(_ value:Double) -> Double {
        return log10(1 + value * 9);
    }
}
