//
//  ContentView.swift
//  YAAE
//
//  Created by Thiago Santos on 05/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import SwiftUI
import Charts

struct RootView : View {
    
    
    var body: some View {
         AnalyserView()
    }
}

#if DEBUG
struct RootView_Previews : PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
#endif
