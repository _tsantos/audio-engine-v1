//
//  Color.swift
//  YAAE
//
//  Created by Thiago Santos on 08/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation
import SwiftUI

struct ChartColors {
    let grid = UIColor(rgb: 0x415A77)
    let line = UIColor(rgb: 0x7286FF)
    let background = UIColor(rgb: 0x2C3E50)
    let text = UIColor(rgb: 0x34495E)
}

struct LayoutColors {
    
    let background = Color.init(rgb: 0x2C3E50)
    let title = Color.init(rgb: 0xE0E1DD)
    let text = Color.init(rgb: 0x778DA9)
    let tile = Color.init(rgb: 0xE7ECEF)
}

extension Color {
    static let layout = LayoutColors()
}

extension UIColor {
    static let chart = ChartColors()
}

/*
  Convert hex to RGB
 */

extension UIColor {
    convenience init(rgb: Int, alpha: CGFloat = 1) {
        self.init(
            red: CGFloat((rgb >> 16) & 0xFF) / 256,
            green: CGFloat((rgb >> 8) & 0xFF) / 256,
            blue: CGFloat(rgb & 0xFF) / 256,
            alpha: alpha
        )
    }
}

extension Color {
    
    init(rgb: Int, opacity: Double = 1) {
        self.init(
            red: Double((rgb >> 16) & 0xFF) / 256,
            green: Double((rgb >> 8) & 0xFF) / 256,
            blue: Double(rgb & 0xFF) / 256,
            opacity: opacity
        )
    }
}
