//
//  Font.swift
//  YAAE
//
//  Created by Thiago Santos on 08/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


import Foundation
import SwiftUI

extension Font {
    
    static let h1: CGFloat = 40
    static let h2: CGFloat = 32
    static let h3: CGFloat = 24
    static let h4: CGFloat = 18
    static let h5: CGFloat = 14
}
