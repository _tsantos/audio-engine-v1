//
//  ContentView.swift
//  YAAE
//
//  Created by Thiago Santos on 05/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import SwiftUI
import Charts

struct AnalyserView : View {
    
    @State var frequencyZoom: Double = 1000000
    @State var spectrumPrecision: Double = 0.1

    @ObjectBinding var signalViewModel = SignalViewModel()
    
    var header: some View {
        Group {
            ZStack {
                HStack {
                    Image("back-button").resizable().frame(width: 40.0, height: 40.0).padding([.leading, .top])
                    Spacer()
                }
                HStack{
                    Image("wave").resizable()
                        .frame(width: 32.0, height: 20.0)
                    Text("Wave Analyser")
                        .font(.system(size: Font.h3)).color(Color.layout.title)
                    Image("wave").resizable()
                        .frame(width: 32.0, height: 20.0)
                }.padding([.top])
            }
        }
    }
    
    var frequencyArea: some View {
        Group {
            HStack {
                VStack {
                    FrequencyChart(signal: signalViewModel.signal, zoom: self.frequencyZoom)
                    Text("ZOOM ")
                        .font(.system(size: Font.h4))
                        .color(Color.layout.text).bold()
                }
            }
        }.background(Color.layout.tile).padding()
        
    }
    
    
    var body: some View {
        VStack {
            self.header
            HStack {
                self.frequencyArea
                self.frequencyArea
            }
        }.background(Color.layout.background)
         .edgesIgnoringSafeArea(.all)
    }
}

#if DEBUG
struct AnalyserView_Previews : PreviewProvider {
    static var previews: some View {
        AnalyserView()
    }
}
#endif
