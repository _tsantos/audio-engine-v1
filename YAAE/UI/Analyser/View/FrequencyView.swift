//
//  SULineChart.swift
//  YAAE
//
//  Created by Thiago Santos on 06/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation
import SwiftUI
import Charts


struct FrequencyChart: UIViewRepresentable {
  
    typealias UIViewType = LineChartView
    
    private let signal: AudioNode
    private let resolution: Int
    private let zoom: Double
    
    init(signal: AudioNode, resolution: Int = 1000, zoom: Double = 100000) {
        self.signal = signal
        self.zoom = zoom
        self.resolution = resolution
    }
   
    func makeUIView(context: UIViewRepresentableContext<FrequencyChart>) -> LineChartView {
        let chart = self.setupChartLayout(chart: LineChartView())
        chart.data = buildData(resolution: self.resolution, zoom: self.zoom)
        return chart
    }
    
    func updateUIView(_ chart: LineChartView, context: UIViewRepresentableContext<FrequencyChart>) {
        chart.data = buildData(resolution: self.resolution, zoom: self.zoom)
        chart.animate(xAxisDuration: 1.5, easingOption: ChartEasingOption.easeInOutElastic)
        chart.notifyDataSetChanged()
    }
    
    private func buildData(resolution: Int, zoom: Double) -> LineChartData {
        var data = Array<Double>()
        let label = "Signal"

        for time in 0...resolution {
            data.append(signal.buildAtTime(timeInSeconds: Double(time) / zoom, currentAmplitude: 0))
        }
        
        let entries = data.enumerated().map { (index, item) in ChartDataEntry(x: Double(index), y: Double(item))  }
        
        return LineChartData(dataSets: [self.setupLineLayout(line: LineChartDataSet(entries: entries, label: label))])
    }
    
    private func setupLineLayout(line: LineChartDataSet) -> LineChartDataSet {
        line.colors = [UIColor.chart.line]
        line.lineWidth = 2.0
        line.valueTextColor = UIColor.chart.text
        line.mode = LineChartDataSet.Mode.linear
        line.drawCirclesEnabled = false
        return line
    }
    
    private func setupChartLayout(chart: LineChartView) -> LineChartView {
        chart.legend.enabled = true
        chart.xAxis.labelPosition = .bottom
        chart.xAxis.labelTextColor = UIColor.chart.text
        chart.leftAxis.labelTextColor = UIColor.chart.text
        chart.rightAxis.labelTextColor = UIColor.chart.text
        chart.gridBackgroundColor = UIColor.chart.background
        chart.drawGridBackgroundEnabled = true
        return chart
    }
}
