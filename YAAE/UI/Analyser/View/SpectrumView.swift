//
//  SUBarChart.swift
//  YAAE
//
//  Created by Thiago Santos on 06/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation
import SwiftUI
import Charts

struct SpectrumChart: UIViewRepresentable {
    
    typealias UIViewType = BarChartView
    
    private let spectrum: FFT.Spectrum
    private let precision: Double
    
    init(spectrum: FFT.Spectrum, precision: Double) {
        self.spectrum = spectrum
        self.precision = precision
    }
    
    func makeUIView(context: UIViewRepresentableContext<SpectrumChart>) -> BarChartView {
        
        let chart = self.setupChartLayout(chart: BarChartView())
        chart.data = buildData(precision: precision)
        return chart
    }
    
    func updateUIView(_ chart: BarChartView, context: UIViewRepresentableContext<SpectrumChart>) {
        chart.data = buildData(precision: precision)
        chart.notifyDataSetChanged()
    }
    
    private func buildData(precision: Double) -> BarChartData {
        let label = "Frequencies"
        
        let entries = self.spectrum
            .filter { entry in entry.amplitude > precision }
            .map { entry in BarChartDataEntry(x: entry.frequency, y: entry.amplitude) }
        
        return BarChartData(dataSets: [self.setupBarLayout(bar: BarChartDataSet(entries: entries, label: label))])
    }
    
    private func setupBarLayout(bar: BarChartDataSet) -> BarChartDataSet {
        bar.colors = [NSUIColor.chart.line]
        bar.barBorderColor = NSUIColor.chart.line
        bar.valueTextColor = UIColor.chart.text
        bar.barBorderWidth  = 3
        return bar
    }
    
    private func setupChartLayout(chart: BarChartView) -> BarChartView {
        
        chart.legend.textColor = UIColor.chart.text
        chart.xAxis.labelPosition = .bottom
        chart.xAxis.wordWrapEnabled = true
        chart.xAxis.labelTextColor = UIColor.chart.text
        chart.leftAxis.labelTextColor = UIColor.chart.text
        chart.rightAxis.labelTextColor = UIColor.chart.text
        chart.backgroundColor = UIColor.chart.background
        
        return chart
    }
    
}
