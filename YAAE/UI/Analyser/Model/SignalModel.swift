//
//  FrequencyModel.swift
//  YAAE
//
//  Created by Thiago Santos on 07/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


class SignalModel {

    private let fft = FFT()
    
    func signal() -> AudioNode {
        return SineSignal(frequency: 16000, volume: 1)
    }
    
    func spectrum(signal: AudioNode) -> FFT.Spectrum {
        return self.fft.spectrum(audioNode: signal)
    }
}
