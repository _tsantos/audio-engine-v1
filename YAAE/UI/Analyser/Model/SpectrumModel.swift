//
//  SpectrumModel.swift
//  YAAE
//
//  Created by Thiago Santos on 07/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation


class SpectrumModel {
    
    let fft = FFT()
    
    func spectrum(audioNode: AudioNode) -> FFT.Spectrum {
        return self.fft.spectrum(audioNode: audioNode)
    }
}

