//
//  FrequencyViewModel.swift
//  YAAE
//
//  Created by Thiago Santos on 07/07/19.
//  Copyright © 2019 Thiago Medeiros dos Santos. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

class SignalViewModel: BindableObject {
    
    typealias PublisherType = PassthroughSubject<SignalViewModel, Never>

    let signalModel = SignalModel()
    
    var didChange = PassthroughSubject<SignalViewModel, Never>()
    
    var signal: AudioNode {
        didSet {
            didChange.send(self)
        }
    }
    
    var spectrum: FFT.Spectrum {
        didSet {
            didChange.send(self)
        }
    }

    init() {
        self.signal = self.signalModel.signal()
        self.spectrum = self.signalModel.spectrum(signal: self.signal)
    }
}
